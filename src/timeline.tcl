namespace eval timeline {
    proc timeline {label slice_list} {
	# Return a timeline object
	#
	# Arguments:
	#   label -- Label for the timeline
	#   slice_list -- List of timeslice objects in the order they'll appear
	set timeline_dict [dict create]
	dict set timeline_dict label $label
	dict set timeline_dict slice_list $slice_list
	return $timeline_dict
    }
}
