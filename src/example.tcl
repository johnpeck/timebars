namespace eval example {

    proc example {infile} {
	# Write an example file to input/<infile>
	#
	# Arguments:
	#   infile -- Filename for the example input
	global log
	global program_name

	# Open the output file.  Make sure the input directory exists.
	try {
	    file mkdir input
	    set fp [open input/$infile w]
	} trap {} {msg o} {
	    ${log}::error $msg
	    exit 1
	}

	
	puts $fp "# Hey Emacs, use -*- Tcl -*- mode"
	puts $fp ""
	puts $fp "# Example $program_name input file"
	puts $fp ""
	set data "# Seconds per inch scaling.  Reduce this to make timeslice labels fit."
	puts $fp $data
	puts $fp "set seconds_per_inch 0.02"
	puts $fp ""
	puts $fp "# Create a timeslice"
	puts $fp "#"
	puts $fp "# Usage: timeslice::timeslice duration color label"
	puts $fp "# Arguments:"
	puts $fp "#   duration -- Length of the timeslice (seconds)"
	puts $fp "#   color -- Common name for the timeslice color (blue, red, ...)"
	puts $fp "#   label -- Text to display inside the timeslice (if it will fit)"
	set data {set mario_airborne [timeslice::timeslice }
	append data {100e-3 red "Mario has jumped"]}
	puts $fp $data
	puts $fp ""
	puts $fp "# Create another timeslice"
	set data {set mario_recovers [timeslice::timeslice }
	append data {50e-3 green "Mario recovers"]}
	puts $fp $data
	puts $fp ""
	set data "# Define a reference line.  This will go in the timeslice list \n"
	append data "# where you'd like a time reference line."
	puts $fp $data
	set data {set mario_lands [reference::reference "Mario lands on goomba"]}
	puts $fp $data
	puts $fp ""
	set data "# Add the timeslices to a list.  We'll use this list to create a \n"
	append data "# timeline."
	puts $fp $data
	set data {set mario_list [list $mario_airborne $mario_lands $mario_recovers]}
	puts $fp $data
	puts $fp ""
	set data "# Create a timeline from this list of timeslices"
	puts $fp $data
	set data {set mario_timeline [timeline::timeline "Mario" $mario_list]}
	puts $fp $data
	puts $fp ""
	puts $fp "# Create a second timeline, starting with more timeslices"
	set data {set goomba_appears [timeslice::timeslice }
	append data {40e-3 blue "Goomba appears"]}
	puts $fp $data
	puts $fp ""
	set data {set goomba_disappears [timeslice::timeslice }
	append data {40e-3 white "Goomba disappears"]}
	puts $fp $data
	puts $fp ""
	set data {set goomba_list [list $goomba_appears $goomba_disappears $goomba_appears]}
	puts $fp $data
	puts $fp ""
	set data {set goomba_timeline [timeline::timeline "Goomba" $goomba_list]}
	puts $fp $data
	puts $fp ""

	set data "# Add the timelines you want to diagram to the timeline_list list"
	puts $fp $data
	set data {set timeline_list [list $mario_timeline $goomba_timeline]}
	puts $fp $data

	puts $fp ""
	puts $fp "# Create key box entries"
	puts $fp {set airborne_keybox [keybox::keybox red "Mario airborne"]}
	puts $fp {set grounded_keybox [keybox::keybox green "Mario grounded"]}
	puts $fp ""
	puts $fp "# Add the key boxes you want to display to the keybox_list list"
	puts $fp {set keybox_list [list $airborne_keybox $grounded_keybox]}
	
	
	
	close $fp
    }

    
}
