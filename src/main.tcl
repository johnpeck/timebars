#!/opt/ActiveTcl-8.6/bin/tclsh
# Hey Emacs, use -*- Tcl -*- mode

# The name of this program.  This will get used to identify logfiles,
# configuration files and other file outputs.
set program_name "timebars"

# The base filename for the execution log.  The actual filename will add
# a number after this to make a unique logfile name.
set execution_logbase "timebars"

# This software's version.  Anything set here will be clobbered by the
# makefile when starpacks are built.
set revcode 1.0

# Set the log level.  Known values are:
# debug
# info
# notice
# warn
# error
# critical
# alert
# emergency
set loglevel debug

# Create a dictionary to keep track of global state
# State variables:
#   program_name --  Name of this program (for naming the window)
#   program_version -- Version of this program
#   thisos  -- Name of the os this program is running on
#   exelog -- The execution log filename
#   serlog -- The serial output log filename
set state [dict create \
	       program_name $program_name \
	       program_version $revcode \
	       thisos $tcl_platform(os) \
	       exelog none \
	       serlog none
	  ]

####################### Tools for all modules ########################
source module_tools.tcl

# ---------------------- Command line parsing -------------------------
package require cmdline

# The program name will likely not be the name of this file
set argv0 $program_name

set usage "usage: $program_name \[options] filename"
set options {
    {d.arg 50 "Minimum depth"}
    {o.arg "timebars.fig" "Output file name"}
}

try {
    array set params [cmdline::getoptions argv $options $usage]
} trap {CMDLINE USAGE} {msg o} {
    # Trap the usage signal, print the message, and exit the application.
    # Note: Other errors are not caught and passed through to higher levels!
    puts $msg
    exit 1
}

if { [string equal $params(o) "timebars.fig"] } {
    # Output file hasn't been specified
    set outfile timebars.fig
} else {
    set outfile $params(o)
}

####################### Set up console logger ########################

package require logger
source loggerconf.tcl
${log}::info [modinfo logger]

# Functions for working with example input
source example.tcl

# Functions for working with timeslices
source timeslice.tcl

# Functions for working with reference lines
source reference.tcl

# Funtions for working with timelines
source timeline.tcl

# Functions for working with keyboxes
source keybox.tcl

# General purpose functions
source utils.tcl

# pi constant
set pi 3.1415926535897932385

# Pixels per inch
set pixels_per_inch 1200

set header "
#FIG 3.2  Produced by xfig version 3.2.5c
Landscape
Center
Inches
Letter
100.00
Single
-2
$pixels_per_inch 2
"

# Set colors starting with 32
set colors "
0 32 #ff0000
0 33 #00ff00
0 34 #c2c2c3
"

proc write_lines {file_pointer data} {
    set linelist [split $data "\n"]
    foreach line $linelist {
	if {[string length $line] > 0} {
	    # Refuse to write blank lines
	    puts $file_pointer $line
	}
    }
}

proc axis_arrow_block {x_pos y_pos} {
    # Draw an axis arrow from the origin
    #
    # Arguments:
    #   x_pos -- x-position of arrowhead (xfig units)
    #   y_pos -- y-position of arrowhead (xfig units)
    global params
    global pixels_per_inch
    global infile::seconds_per_inch
    global timeline_width
    # Set the axis line width here -- arrowhead will scale
    set linewidth 2
    # 2 -- Object code (always 2)
    # 1 -- Object subtype (1 for polyline)
    # 0 -- Line style (0 for solid)
    # 1 -- Line thickness (1/80 inch units)
    # 0 -- Pen color (0 for black)
    # 7 -- Fill color (0 for black, 7 for white)
    set arrowblock "2 1 0 $linewidth 0 7 "
    # Set the depth to be 10 above the boxes
    append arrowblock "[expr $params(d) - 10] "
    # -1 -- Pen style (not used)
    # -1 -- Area fill
    # 0.000 -- Style value
    # 0 -- Join style
    # 0 -- Cap style
    # -1 -- Radius of arc-box (-1 turns this off)
    # 1 -- Forward arrow (0 for off)
    # 0 -- Backward arrow (0 for off)
    # 5 -- Points in the polyline
    append arrowblock "-1 -1 0.000 0 0 -1 1 0 2\n"
    append arrowblock "\t"

    # Set the arrow parameters
    # 1 -- Arrow type
    # 1 -- Arrow style
    # 1.00 -- Arrow thickness (1/80 inches)
    # 60.00 -- Arrow width (fig units)
    # 60.00 -- Arrow height (fig units)
    set arrowdims [expr 60.00 * $linewidth]
    append arrowblock "1 1 1.00 $arrowdims $arrowdims\n"
    append arrowblock "\t "

    # Set start and end coordinates
    append arrowblock "0 0 $x_pos $y_pos"
    return $arrowblock
}

proc deg_to_rad {degrees} {
    # Return the radian value
    #
    # Arguments:
    #   degrees -- Angle in degrees
    set pi 3.1415926535897932385
    set radians [expr $degrees * $pi/180]
    return $radians
}

proc rad_to_deg {radians} {
    # Return the degree value
    #
    #
    # Arguments:
    #   radians -- Angle in radians
    set pi 3.1415926535897932385
    set degrees [expr $radians * 180/$pi]
    return $degrees
}

proc timeline_label_line {y_pos text} {
    # Return the xfig file line for a line of text formatted as a
    # timeline axis label at the given position
    #
    # Arguments:
    #
    #   y_pos -- y-position of text anchor (xfig units)
    #   text -- Label text
    global params
    set fontsize 12
    # 4 -- Object code (always 4)
    # 2 -- Object subtype (2 for right justified)
    # 0 -- Pen color (0 for black)
    # ? -- Depth (from params)
    # -1 -- Pen style (not used)
    # 16 -- Font (16 for Helvetica)
    # ? -- Font size
    set textline "4 2 0 $params(d) -1 16 $fontsize "
    # Set the text angle in radians
    append textline "[deg_to_rad 0] "
    # 4 -- Font flags (4 for postscript)
    append textline "4 "
    # Set the height and length.  I have no idea how to do this.  A
    # height of 150 is good for 12-point Helvetica.  A scale factor of
    # 225 is good for 12-point Helvetica.
    append textline "150 [expr [string length $text] * 225] "
    # Set the x and y position
    append textline "-100 $y_pos "
    # Add the text
    append textline "${text}\\001"
    return $textline
}

proc add_time_reference {time_reference_list time_s label} {
    # Return the list of time references with a this new addition
    #
    # Arguments:
    #   time_s -- Time in seconds
    #   label -- The label for the time reference
    set new_reference [dict create]
    dict set new_reference time_s $time_s
    dict set new_reference label $label
    lappend time_reference_list $new_reference
    return $time_reference_list
}



proc get_bounding_box {boxline_list} {
    # Return four coordinates to define the drawing's bounding box
    #
    # Arguments:
    #   boxline_list -- List of lines in the drawing
    foreach line $boxline_list {
	set coords_string [string trim [lindex [split $line "\n"] 1]]
	foreach {x y} $coords_string {
	    if [info exists xmin] {
		if {$x < $xmin} {
		    set xmin $x
		}
	    } else {
		set xmin $x
	    }
	    if [info exists xmax] {
		if {$x > $xmax} {
		    set xmax $x
		}
	    } else {
		set xmax $x
	    }
	    if [info exists ymin] {
		if {$y < $ymin} {
		    set ymin $y
		}
	    } else {
		set ymin $y
	    }
	    if [info exists ymax] {
		if {$y > $ymax} {
		    set ymax $y
		}
	    } else {
		set ymax $y
	    }
	}
    }
    return "$xmin $ymin $xmax $ymax"
}

# After cmdline is done, argv will point to the last argument
if {[llength $argv] == 1} {
    set infile $argv
} else {
    # Input file hasn't been specified.  Create input at input/example.tcl
    ${log}::info "No input file has been specified.  Creating example at input/example.tbi"
    # Note that this example file is just a tcl file, but my makefile
    # won't let me give it a tcl extension.  A tbi extension is good enough.
    example::example example.tbi
    set infile input/example.tbi
}

# Try sourcing the input file
try {
    ${log}::info "Reading input file $infile"
    namespace eval infile {
	source $infile
    }
} trap {} {message optdict} {
    ${log}::error "Failed to source $infile"
    ${log}::error $errorInfo
    exit
}

# See if seconds_per_inch is set
if {[info exists infile::seconds_per_inch]} {
    ${log}::debug "Found seconds_per_inch"
} else {
    ${log}::warn "seconds_per_inch not found in $infile.  Defaulting to 0.01"
    set infile::seconds_per_inch 0.01
    exit
}

# Make sure timeline_list exists
if {[info exists infile::timeline_list]} {
    ${log}::debug "Found timeline_list"
} else {
    ${log}::error "timeline_list not found in $infile"
    exit
}

# Open the output file.  Make sure the output directory exists.
try {
    file mkdir output
    set fp [open output/$outfile w]
    ${log}::info "Writing output to output/$outfile"
} trap {} {msg o} {
    puts $msg
    exit 1
}

# Write the file header
write_lines $fp $header

# Write the custom colors
write_lines $fp $colors

# First timeline will be y_pos pixels above zero
set initial_y_pos [expr round(-0.5 * $pixels_per_inch)]

set initial_x_pos [expr round(0.5 * $pixels_per_inch)]

# Spacing between timelines
set y_increment [expr round(1 * $pixels_per_inch)]

# Timeline width
set timeline_width [expr round($timeslice::height_inch * $pixels_per_inch)]
set timeline_half_width [expr round(0.5 * $timeline_width)]

set y_pos $initial_y_pos
set x_pos $initial_x_pos

# List of time references (to be made into vertical lines with labels)
set time_reference_list [dict create]

set figure_line_list [list]
set box_line_list [list]
set y_label_line_list [list]

# Make sure seconds per inch was set in the input file
if {[info exists infile::seconds_per_inch]} {
    ${log}::debug "Found seconds_per_inch"
} else {
    ${log}::error "seconds_per_inch scale factor not found in $infile"
    exit
}

foreach timeline $infile::timeline_list {
    set elapsed_s 0
    # Add the timeline label
    lappend y_label_line_list \
	[timeline_label_line [expr $y_pos - $timeline_half_width + 75] \
	     [dict get $timeline label]]
    # Add all timeslices for the current timeline
    foreach timeslice_dict [dict get $timeline slice_list] {

	if {[string equal [dict get $timeslice_dict type] timeslice]} {
	    lappend box_line_list [timeslice::block $x_pos $y_pos $timeslice_dict]
	    incr x_pos [expr round([dict get $timeslice_dict duration] / \
				       double($infile::seconds_per_inch) * \
				       $pixels_per_inch)]
	    set elapsed_s [expr $elapsed_s + [dict get $timeslice_dict duration]]
	}
	if {[string equal [dict get $timeslice_dict type] reference]} {
	    # This is a reference-type timeslice.  Create a label for
	    # the reference using the original label and the elapsed time.
	    set reference_label [dict get $timeslice_dict label]
	    append reference_label " -- [join [utils::eng_format $elapsed_s]]"
	    set time_reference_list \
		[add_time_reference $time_reference_list $elapsed_s $reference_label]

	}

    }
    set x_pos $initial_x_pos
    incr y_pos -$y_increment
}
foreach line $box_line_list {
    write_lines $fp $line
}
set timebars_bounding_box [get_bounding_box $box_line_list]
set x_box_extreme [lindex $timebars_bounding_box 2]
set y_box_extreme [lindex $timebars_bounding_box 1]

# Create key boxes
set y_pos $y_box_extreme
set x_pos $initial_x_pos
foreach keybox $infile::keybox_list {
    incr y_pos -[expr round($xfig::pixels_per_inch * \
			       ($keybox::spacing_inch + $keybox::height_inch))]
    lappend keybox_line_list [keybox::block $x_pos $y_pos $keybox]
}

set axis_line_list [list]
lappend axis_line_list [axis_arrow_block \
			    [expr round($x_box_extreme + 500)] 0]
lappend axis_line_list [axis_arrow_block \
			    0 [expr round($y_box_extreme - 500)]]

foreach line $keybox_line_list {
    write_lines $fp $line
}

# Draw arrows for the axes
foreach line $axis_line_list {
    write_lines $fp $line
}

# Add timeline labels
foreach line $y_label_line_list {
    write_lines $fp $line
}

foreach reference $time_reference_list {
    write_lines $fp [reference::block $box_line_list $reference]
}

close $fp
