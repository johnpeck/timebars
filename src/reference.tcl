namespace eval reference {

    proc reference {label} {
	# Return a reference-type object
	#
	# Arguments:
	#   label -- Label
	set reference_dict [dict create]
	dict set reference_dict type reference
	dict set reference_dict label $label
	return $reference_dict
    }

    proc block {box_line_list time_reference} {
	# Return an xfig file block for a vertical line time reference
	#
	# Arguments:
	#   box_line_list -- List of xfig lines forming the timeline blocks
	#   time_reference -- Time reference dictionary
	global pixels_per_inch
	global infile::seconds_per_inch
	global initial_x_pos
	set time_s [dict get $time_reference time_s]
	set label [dict get $time_reference label]
	set x_pos [expr $initial_x_pos + \
		       round(double($time_s)/$infile::seconds_per_inch * $pixels_per_inch)]
	set timebars_bounding_box [get_bounding_box $box_line_list]
	# There will just be one y position -- the other will be 0
	set y_pos [lindex $timebars_bounding_box 1]
	set reference_block [reference::line_block $x_pos 0 $y_pos]
	append reference_block "\n[reference::label_block $x_pos $label]"
	return $reference_block
    }

    proc line_block {x_pos y_low_pos y_high_pos} {
	# Return the xfig file block adding a vertical dashed line
	#
	# Arguments:
	#   x_pos -- x position of the line (xfig coordinates)
	#   y_low_pos -- lower y position of the line (xfig coordinates)
	#   y_high_pos -- higher y position of the line (xfig coordinates)
	global params
	# Set the line width
	set linewidth 1
	# 2 -- Object code (always 2)
	# 1 -- Object subtype (1 for polyline)
	# 1 -- Line style (1 for dashed)
	# 1 -- Line thickness (1/80 inch units)
	# 0 -- Pen color (0 for black)
	# 7 -- Fill color (0 for black, 7 for white)
	set lineblock "2 1 1 $linewidth 0 7 "
	# Set the depth to be 1 above the boxes
	append lineblock "[expr $params(d)-1] "
	# -1 -- Pen style (not used)
	# -1 -- Area fill
	# 4.000 -- Length of on/off dashes
	# 0 -- Join style
	# 0 -- Cap style
	# -1 -- Radius of arc-box (-1 turns this off)
	# 0 -- Forward arrow (0 for off)
	# 0 -- Backward arrow (0 for off)
	# 5 -- Points in the polyline
	append lineblock "-1 -1 4.000 0 0 -1 0 0 2\n"
	append lineblock "\t"

	# Set start and end coordinates
	append lineblock "$x_pos $y_low_pos $x_pos $y_high_pos"
	return $lineblock
    }

    proc label_block {x_pos label} {
	# Return the xfig file block adding a time reference label
	#
	# Arguments:
	#   x_pos -- x-position of the label (y will be constant)
	#   label -- The label text
	global params
	set fontsize 12
	# 4 -- Object code (always 4)
	# 2 -- Object subtype (2 for right justified)
	# 0 -- Pen color (0 for black)
	set textline "4 2 0 "
	# Set the depth to be above the boxes but below axis arrows
	append textline "[expr $params(d) -1] "
	# -1 -- Pen style (not used)
	# 16 -- Font (16 for Helvetica)
	# ? -- Font size
	append textline " -1 16 $fontsize "
	# Set the text angle in radians
	append textline "[deg_to_rad 90] "
	# 4 -- Font flags (4 for postscript)
	append textline "4 "
	# Set the height and length.
	append textline "[xfig::char_height helvetica $fontsize] "\
	    "[expr [string length $label] * [xfig::char_width helvetica $fontsize]] "
	# Set the x and y position
	append textline "[expr $x_pos + 75] 100 "
	# Add the text
	append textline "${label}\\001"
	return $textline
    }
}
