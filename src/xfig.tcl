namespace eval xfig {

    # Pixels per inch
    variable pixels_per_inch 1200

    proc color_codes {name} {
	# Return the color code corresponding to the common name
	#
	# Find the basic color mapping at
	# http://homepage.usask.ca/~ijm451/fig/FORMAT3.2.txt
	#
	# Arguments:
	#   name -- Common name for the color, like "blue"
	if {[string equal -nocase $name black]} {
	    return 0
	}
	if {[string equal -nocase $name blue]} {
	    return 1
	}
	if {[string equal -nocase $name green]} {
	    return 2
	}
	if {[string equal -nocase $name cyan]} {
	    return 3
	}
	if {[string equal -nocase $name red]} {
	    return 4
	}
	if {[string equal -nocase $name magenta]} {
	    return 5
	}
	if {[string equal -nocase $name yellow]} {
	    return 6
	}
	if {[string equal -nocase $name white]} {
	    return 7
	}

    }

    proc char_height {font size} {
	# Return an average character height for a given font name and
	# size
	#
	# Arguments:
	#   font -- Name of font, like helvetica
	#   size -- Size of font, like 12
	if {[string equal -nocase $font helvetica]} {
	    if {$size == 12} {
		return 100
	    }
	}
	return 100
    }

    proc char_width {font size} {
	# Return an average character width for a given font name and
	# size
	#
	# Arguments:
	#   font -- Name of font, like helvetica
	#   size -- Size of font, like 12
	if {[string equal -nocase $font helvetica]} {
	    if {$size == 12} {
		return 100
	    }
	}
	return 100
    }

    proc mils_to_fig {mils} {
	# Return the fig length
	#
	# Arguments:
	#   mils -- Length in mils (0.001 inch)
	set figlength [expr round($xfig::pixels_per_inch * double($mils)/1000)]
	return $figlength
    }

}
