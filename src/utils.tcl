namespace eval utils {


    proc deg_to_rad {degrees} {
	# Return the radian value
	#
	# Arguments:
	#   degrees -- Angle in degrees
	set pi 3.1415926535897932385
	set radians [expr $degrees * $pi/180]
	return $radians
    }

    proc char_width {font size} {
	# Return an average character width for a given font name and
	# size
	#
	# Arguments:
	#   font -- Name of font, like helvetica
	#   size -- Size of font, like 12
	if {[string equal -nocase $font helvetica]} {
	    if {$size == 12} {
		return 100
	    }
	}
	return 100
    }

    proc char_height {font size} {
	# Return an average character height for a given font name and
	# size
	#
	# Arguments:
	#   font -- Name of font, like helvetica
	#   size -- Size of font, like 12
	if {[string equal -nocase $font helvetica]} {
	    if {$size == 12} {
		return 100
	    }
	}
	return 100
    }

    proc eng_format {number} {
	# Return the number formatted in engineering notation.
	#
	# Arguments:
	#   number -- Floating point number
        array set orders {
         -8 y -7 z -6 a -5 f -4 p -3 n -2 u -1 m 0 {} 1 k 2 M 3 G 4 T 5 P 6 E 7 Z 8 Y
        }
        set numInfo  [split [format %e $number] e]
        set order [expr {[scan [lindex $numInfo 1] %d] / 3}]
        if {[catch {set orders($order)} prefix]} {return [list $number]}
        set number [expr {$number/pow(10,3*$order)}]
        return [list [format "%0.1f" $number] $prefix]
    }
}
