source xfig.tcl

namespace eval timeslice {

    # Height of individual timeslices (inches)
    set height_inch 0.5

    set height [expr $height_inch * $xfig::pixels_per_inch]
    
    proc timeslice {duration color label} {
	# Return a timeslice object
	#
	# Arguments:
	#   duration -- Length of timeslice (floating point seconds)
	#   color -- Color for the timeslice
	#   label -- Label to put in the timeslice (if it fits)
	set timeslice_dict [dict create]
	dict set timeslice_dict type timeslice
	dict set timeslice_dict duration [expr $duration]
	dict set timeslice_dict color $color
	dict set timeslice_dict label $label
	return $timeslice_dict
    }

    proc reference {label} {
	# Return a reference-type object
	#
	# Arguments:
	#   label -- Label
	set reference_dict [dict create]
	dict set reference_dict type reference
	dict set reference_dict label $label
	return $reference_dict
    }

    proc block {x_pos y_pos timeslice} {
	# Return the xfig file block for a timeslice
	#
	# Arguments:
	#   x_pos -- Lower left corner of the box x position (xfig units)
	#   y_pos -- Lower left corner of the box y position (xfig units)
	#   timeslice -- Timeslice object
	global params
	global infile::seconds_per_inch
	global timeline_width
	# Set the color tint from 21-39, where 39 is the lightest tint
	set color_saturation 30
	# 2 -- Polyline object code
	# 1 -- Polyline subtype
	# 0 -- Line style
	# 1 -- Thickness
	# 0 -- Pen color
	# xx -- Fill color
	set color [dict get $timeslice color]
	set duration [expr round([dict get $timeslice duration] / \
				     double($infile::seconds_per_inch) * \
				     $xfig::pixels_per_inch)]
	set colorcode [xfig::color_codes $color]
	set boxblock "2 1 0 1 0 $colorcode "
	# Set the depth
	append boxblock "$params(d) "
	# -1 -- Pen style (not used)
	append boxblock "-1 "
	# ? -- Area fill (values 21-39 are progressively lighter tints)
	append boxblock "$color_saturation "
	# 0.000 -- Style value
	# 0 -- Join style
	# 0 -- Cap style
	# -1 -- Radius of arc-box (-1 turns this off)
	# 0 -- Forward arrow (0 for off)
	# 0 -- Backward arrow (0 for off)
	# 5 -- Points in the polyline
	append boxblock "0.000 0 0 -1 0 0 5\n"
	append boxblock "\t "

	# Upper right corner
	set x1 [expr $duration + $x_pos]
	set y1 [expr $y_pos - $timeline_width]
	append boxblock "$x1 $y1 "
	# Upper left corner
	set x2 $x_pos
	set y2 $y1
	append boxblock "$x2 $y2 "
	# Lower left corner
	set x3 $x_pos
	set y3 $y_pos
	append boxblock "$x3 $y3 "
	# Lower right corner
	set x4 $x1
	set y4 $y_pos
	append boxblock "$x4 $y4 "
	# Upper right corner (again)
	append boxblock "$x1 $y1"
	# Add label
	append boxblock "\n[label_block $x_pos $y_pos $timeslice]"
	return $boxblock
    }

    proc label_block {x_pos y_pos timeslice} {
	# Return the xfig file block for a timeslice label
	#
	# Arguments:
	#   x_pos -- Lower left corner of the box x position (xfig units)
	#   y_pos -- Lower left corner of the box y position (xfig units)
	#   timeslice -- Timeslice object
	global params
	global infile::seconds_per_inch
	global log
	set label [dict get $timeslice label]
	set duration [expr round([dict get $timeslice duration] / \
				     double($infile::seconds_per_inch) * \
				     $xfig::pixels_per_inch)]
	set fontsize 12
	# 4 -- Object code (always 4)
	# 0 -- Object subtype (0 for left justified)
	# 0 -- Pen color (0 for black)
	set textline "4 0 0 "
	# Set the depth to be above the boxes but below axis arrows
	append textline "[expr $params(d) -1] "
	# -1 -- Pen style (not used)
	# 16 -- Font (16 for Helvetica)
	# ? -- Font size
	append textline " -1 16 $fontsize "
	# Set the text angle in radians
	append textline "[utils::deg_to_rad 0] "
	# 4 -- Font flags (4 for postscript)
	append textline "4 "
	# Set the height and length.  I have no idea how to do this.  A
	# height of 150 is good for 12-point Helvetica.  A scale factor of
	# 100 is good for 12-point Helvetica.
	append textline "150 [expr [string length $label] * 100] "
	# Set the x and y position
	set label_width [expr [utils::char_width helvetica $fontsize] * \
			     [string length $label]]
	set label_height [utils::char_height helvetica $fontsize]
	if {$label_width > [expr $duration]} {
	    ${log}::warn "Label $label is too big for timeslice."
	    return ""
	}
	set x_label_pos [expr round($x_pos + ($duration - $label_width)/2)]
	set y_label_pos [expr round($y_pos - ($timeslice::height - $label_height)/2)]
	append textline "$x_label_pos $y_label_pos "
	# Add the text
	append textline "${label}\\001"
	return $textline
    }


    
}
